# @summary Domain or local user with credentials
type Centric8::Account = Struct[
  user               => String,
  Optional[domain]   => String,
  sensitive_password => Sensitive[String],
]
