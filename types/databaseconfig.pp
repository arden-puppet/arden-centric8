# @summary Configuration used in config.txt and standalone-pi-mssql.xml
type Centric8::DatabaseConfig = Struct[
  name               => String,
  server             => Stdlib::Host,
  port               => Integer,
  user               => String,
  sensitive_password => Sensitive[String],
]
