# @summary Internal class which configures services associated with c8
class centric8::service (
  Centric8::Account $service_account = $centric8::service_account,
){
  # Start each service
  $services = {
    'Centric Canvas Thumbnail Service' => 'delayed',
    'CentricImageService'              => 'true',
    'Centric PDF Service'              => 'true',
    'WFAS13SVC'                        => 'true',
    'SMTPSVC'                          => 'true',
  }
  $services.each |$service, $enable| {
    service { $service:
      ensure => 'running',
      enable => $enable,
    }
  }

  # Configure the execution user
  $domain_user = "${service_account['domain']}\\${service_account['user']}"

  # Ensure the account has the logon as a service property
  dsc_userrightsassignment { "${domain_user}_log_on_as_a_service":
    ensure       => 'present',
    dsc_policy   => 'Log_on_as_a_service',
    dsc_identity => $domain_user,
    dsc_force    => false,
    before       => [
      Dsc_service['WFAS13SVC_Service'],
      Service['WFAS13SVC'],
    ],
  }


  dsc_service { 'WFAS13SVC_Service':
    dsc_name       => 'WFAS13SVC',
    dsc_ensure     => 'present',
    dsc_state      => 'running',
    before         => Service['WFAS13SVC'],
    dsc_credential => {
      'user'     => $domain_user,
      'password' => $service_account['sensitive_password'],
    },
  }
}
