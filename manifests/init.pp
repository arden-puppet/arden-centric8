# @param service_account
#   - `user` - Execution user for the 'Wildfly' service.
#   - `domain` - Active directory domain to which $user belongs.
#   - `sensitive_password` - Password for the $user account in $domain.
#
# @param app_admin_account
#   - `user` - Administrator user within Centric8.
#   - `sensitive_password` - Password for the $user.
#
# @param hash_db_config
#   Database parameters used by the application server including:
#     - `name` - The database instance name
#     - `user` - Database credentials used to access the system
#     - `sensitive_password` - credential used to log into the database
#     - `port` - Connection port for the database
#
# @param filevault_path
#   Absolute path to the local filevault store
#
# @param smtp_server
#   Hostname or IP address of the smtp relay to which this system should forward
#   mail.
#
# @param app_server
#   Application server short name that appears to be used for some kind of
#   application level clustering.
#
# @param app_dns_name
#   Fully qualified DNS name which clients use to access the C8 system.
#
# @param app_email
#   Email address used by the application to send outbound mail.
#
# @summary Configures a Centric8 application Server service on a given node.
class centric8 (
  Centric8::Account $service_account,
  Centric8::Account $app_admin_account,
  Centric8::DatabaseConfig $hash_db_config,
  Stdlib::Absolutepath $filevault_path,
  Stdlib::Host $smtp_server,
  Stdlib::Host $app_server,
  Stdlib::Fqdn $app_dns_name,
  String $app_email,
) {
  contain 'centric8::install'
  contain 'centric8::config'
  contain 'centric8::service'

  Class['centric8::install']
  -> Class['centric8::config']
  ~> Class['centric8::service']
}
