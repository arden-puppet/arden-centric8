# @param array_publish_addresses
#   See main class documentation.
#
# @summary Performs zone creation and update operations. Internal class
class centric8::config (
  Centric8::Account $app_admin_account = $centric8::app_admin_account,
  Centric8::DatabaseConfig $hash_db_config  = $centric8::hash_db_config,
  Stdlib::Absolutepath $filevault_path = $centric8::filevault_path,
  Stdlib::Host $smtp_server            = $centric8::smtp_server,
  Stdlib::Host $app_server             = $centric8::app_server,
  Stdlib::Fqdn $app_dns_name   = $centric8::app_dns_name,
  String $app_email                    = $centric8::app_email,
) {
  $c8_path = 'C:/Program Files/Centric Software/C8'
  $wildfly_config_path = "${c8_path}/Wildfly-13.0.0/standalone/configuration"
  $app_server_upper = upcase($app_server)
  $db_server_upper = upcase($hash_db_config['server'])
  # Primary configuration files
  $epp_config_params = {
    app_admin_account => $app_admin_account,
    app_email         => $app_email,
    hash_db_config    => $hash_db_config,
    filevault_path    => $filevault_path,
    smtp_server       => $smtp_server,
  }
  file { "${c8_path}/bin/config.txt":
    ensure  => 'file',
    content => Sensitive(epp('centric8/config.txt.epp', $epp_config_params)),
  }
  $mssql_property_value_map = {
    'com.centricsoftware.AppServer.Database.ServerAddress' => "\${env.C8_AppServer_Database_ServerAddress:${db_server_upper}}",
    'com.centricsoftware.AppServer.Database.Port' => "\${env.C8_AppServer_Database_Port:${hash_db_config['port']}}",
    'com.centricsoftware.AppServer.Database.DBName' => "\${env.C8_AppServer_Database_DBName:${hash_db_config['name']}}",
    'com.centricsoftware.AppServer.Database.Username' => "\${env.C8_AppServer_Database_Username:${hash_db_config['user']}}",
    'com.centricsoftware.AppServer.ClusterNodeAddress' => $app_server_upper,
    'com.centricsoftware.AppServer.WebServer' => $app_dns_name,
    'com.centricsoftware.AppServer.HomeURL' => "https://${app_dns_name}/WebAccess/home.html",
    'com.centricsoftware.AppServer.HomeExternalURL' => "https://${app_dns_name}/WebAccess/home.html",
  }

  # Config entries
  #========== standalone-pi-mssql.xml
  $mssql_property_value_map.each |$property, $value| {
    $name_string = "name=\"${property}\""
    $value_string = "value=\"${value}\""
    $prefix = "        <property ${name_string}"
    $line = "${prefix} ${value_string}/>"
    file_line { "standalone-pi-mssql_${property}":
      ensure => 'present',
      path   => "${wildfly_config_path}/standalone-pi-mssql.xml",
      line   => $line,
      match  => "^${prefix}",
    }
  }
  #========== standalone-pi-mssql-ha-tcp.xml
  # TODO: future / other config files?
  # system-properties section
  # com.centricsoftware.AppServer.Database.ServerAddress
  # com.centricsoftware.AppServer.Database.Port
  # com.centricsoftware.AppServer.Database.DBName
  # com.centricsoftware.AppServer.Database.Username
  # com.centricsoftware.AppServer.ClusterNodeAddress
  # com.centricsoftware.AppServer.WebServer
  # com.centricsoftware.AppServer.WebAccessLocalPath
  # com.centricsoftware.AppServer.HomeURL
  # com.centricsoftware.AppServer.HomeExternalURL

  # pi-configuration.properties
  $pi_configuration_property_value_map = {
    'com.centricsoftware.server.canvassnapshotgenerator.address' => "http://${app_server_upper}:3333",
    'com.centricsoftware.server.reportdb.address'                => "http://${app_server_upper}:8083",
  }
  $pi_configuration_property_value_map.each |$property, $value| {
    file_line { "pi-configuration_${property}":
      ensure => 'present',
      path   => "${wildfly_config_path}/pi-configuration.properties",
      line   => "${property} = ${value}",
      match  => "^${property}\\s",
    }
  }
}
