# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

describe 'centric8' do
  let(:app_admin_account) do
    {
      user: 'Administrator',
      sensitive_password: sensitive('some-secret-password'),
    }
  end
  let(:service_account) do
    {
      user: 'c8-service',
      domain: 'example',
      sensitive_password: sensitive('some-secret-password'),
    }
  end
  let(:hash_db_config) do
    {
      name: 'CSIC8',
      server: 'C8DB01',
      port: 1433,
      user: 'csidba',
      sensitive_password: sensitive('secretest-password'),
    }
  end
  let(:smtp_server) { 'smtp.example.com' }
  let(:app_server) { 'C8APP01' }
  let(:app_email) { 'centric8@example.com' }
  let(:app_dns_name) { 'centric8.example.com' }
  let(:filevault_path) { 'F:\filevault' }
  let(:c8_path) { 'C:/Program Files/Centric Software/C8' }
  let(:params) do
    {
      service_account: service_account,
      app_admin_account: app_admin_account,
      hash_db_config: hash_db_config,
      filevault_path: filevault_path,
      smtp_server: smtp_server,
      app_server: app_server,
      app_dns_name: app_dns_name,
      app_email: app_email,
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'compiles with default parameters' do
        is_expected.to compile
      end

      it 'contains the correct classes and enforces their ordering' do
        is_expected.to contain_class('centric8::install').with(
          before: ['Class[Centric8::Config]'],
        )
        is_expected.to contain_class('centric8::config').with(
          notify: ['Class[Centric8::Service]'],
        )
        is_expected.to contain_class('centric8::service')
      end
    end
  end
end
