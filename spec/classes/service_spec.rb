# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

service_list = {
  'Centric Canvas Thumbnail Service' => 'delayed',
  'Centric PDF Service' => 'true',
  'CentricImageService' => 'true',
  'WFAS13SVC' => 'true',
  'SMTPSVC' => 'true',
}

describe 'centric8::service' do
  let(:params) do
    {
      service_account: {
        user: 'c8-service',
        domain: 'example',
        sensitive_password: sensitive('some-secret-password'),
      },
    }
  end

  it 'compiles' do
    is_expected.to compile
  end

  service_list.each do |service_name, enable_state|
    it "enables '#{service_name}' service" do
      is_expected.to contain_service(service_name).with(
        ensure: 'running',
        enable: enable_state,
      )
    end
  end

  it 'configures log_on_as_a_service rights for the Wildfly user' do
    is_expected.to contain_dsc_userrightsassignment('example\c8-service_log_on_as_a_service').with(
      ensure: 'present',
      dsc_policy: 'Log_on_as_a_service',
      dsc_identity: 'example\c8-service',
      dsc_force: false,
      before: [
        'Dsc_service[WFAS13SVC_Service]',
        'Service[WFAS13SVC]',
      ],
    )
  end

  it 'configures the service account parameters for the Wildfly Service' do
    is_expected.to contain_dsc_service('WFAS13SVC_Service').with(
      dsc_ensure: 'present',
      dsc_name: 'WFAS13SVC',
      dsc_state: 'running',
      before: 'Service[WFAS13SVC]',
      dsc_credential: {
        'user' => 'example\c8-service',
        'password' => sensitive('some-secret-password'),
      },
    )
  end
end
