# frozen_string_literal: true

require 'spec_helper'
require 'deep_merge'

standalone_pi_entries = [
  {
    name: 'com.centricsoftware.AppServer.Database.ServerAddress',
    value: '${env.C8_AppServer_Database_ServerAddress:C8DB01}',
  },
  {
    name: 'com.centricsoftware.AppServer.Database.Port',
    value: '${env.C8_AppServer_Database_Port:1433}',
  },
  {
    name: 'com.centricsoftware.AppServer.Database.DBName',
    value: '${env.C8_AppServer_Database_DBName:CSIC8}',
  },
  {
    name: 'com.centricsoftware.AppServer.Database.Username',
    value: '${env.C8_AppServer_Database_Username:csidba}',
  },
  {
    name: 'com.centricsoftware.AppServer.ClusterNodeAddress',
    value: 'C8APP01',
  },
  {
    name: 'com.centricsoftware.AppServer.WebServer',
    value: 'centric8.example.com',
  },
  {
    name: 'com.centricsoftware.AppServer.HomeURL',
    value: 'https://centric8.example.com/WebAccess/home.html',
  },
  {
    name: 'com.centricsoftware.AppServer.HomeExternalURL',
    value: 'https://centric8.example.com/WebAccess/home.html',
  },
]

pi_configuration_entries = [
  {
    name: 'com.centricsoftware.server.canvassnapshotgenerator.address',
    value: 'http://C8APP01:3333',
  },
  {
    name: 'com.centricsoftware.server.reportdb.address',
    value: 'http://C8APP01:8083',
  },
]

describe 'centric8::config' do
  let(:app_admin_account) do
    {
      user: 'Administrator',
      sensitive_password: sensitive('some-secret-password'),
    }
  end
  let(:hash_db_config) do
    {
      name: 'CSIC8',
      server: 'c8db01',
      port: 1_433,
      user: 'csidba',
      sensitive_password: sensitive('secretest-password'),
    }
  end
  let(:smtp_server) { 'smtp.example.com' }
  let(:app_server) { 'c8app01' }
  let(:app_email) { 'centric8@example.com' }
  let(:app_dns_name) { 'centric8.example.com' }
  let(:filevault_path) { 'F:\filevault' }
  let(:c8_path) { 'C:/Program Files/Centric Software/C8' }
  let(:params) do
    {
      app_admin_account: app_admin_account,
      hash_db_config: hash_db_config,
      filevault_path: filevault_path,
      smtp_server: smtp_server,
      app_server: app_server,
      app_dns_name: app_dns_name,
      app_email: app_email,
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it 'compiles' do
        is_expected.to compile
      end

      it 'generates config.txt' do
        file_content = <<~HEREDOC
        #{hash_db_config[:server].upcase}
        #{hash_db_config[:port]}

        #{hash_db_config[:user]}
        #{hash_db_config[:sensitive_password].unwrap}
        #{hash_db_config[:name]}
        #{app_admin_account[:user]}
        #{app_admin_account[:sensitive_password].unwrap}
        #{smtp_server}
        #{app_email}

        #{filevault_path}



        C:\\PROGRA~1\\CENTRI~1\\C8\\WebAccess
        HEREDOC
        is_expected.to contain_file("#{c8_path}/bin/config.txt").with(
          ensure: 'file',
          content: sensitive(file_content),
        )
      end

      standalone_pi_entries.each do |entry|
        it "configures property '#{entry[:name]}'" do
          name = entry[:name]
          value = entry[:value]
          is_expected.to contain_file_line("standalone-pi-mssql_#{name}").with(
            ensure: 'present',
            path: "#{c8_path}/Wildfly-13.0.0/standalone/configuration/standalone-pi-mssql.xml",
            line: "        <property name=\"#{name}\" value=\"#{value}\"/>",
            match: "^        <property name=\"#{name}\"",
          )
        end
      end

      pi_configuration_entries.each do |entry|
        property = entry[:name]
        value = entry[:value]
        it "configures property '#{property}'" do
          is_expected.to contain_file_line("pi-configuration_#{property}").with(
            ensure: 'present',
            path: "#{c8_path}/Wildfly-13.0.0/standalone/configuration/pi-configuration.properties",
            line: "#{property} = #{value}",
            match: "^#{property}\\s",
          )
        end
      end
    end
  end
end
