# centric8

[![pipeline status](https://gitlab.com/arden-puppet/arden-centric8/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-centric8/commits/master) [![Version](https://img.shields.io/puppetforge/v/arden/centric8.svg)](https://forge.puppet.com/arden/centric8) [![coverage report](https://gitlab.com/arden-puppet/arden-centric8/badges/master/coverage.svg)](https://gitlab.com/arden-puppet/arden-centric8/commits/master)

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with centric8](#setup)
    * [What centric8 affects](#what-centric8-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with centric8](#beginning-with-centric8)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

This module provides a basic level of configuration control for a Centric8 PLM
instance. Note that it only configures settings on an application server node
and requires the application to be installed first.

## Setup

### What centric8 affect

This module performs the following configuration as it relates to Centric8
version 6.6:

* Ensures all relevant Windows services are enabled and running.
* Configures the Wildfly service using the provided credentials.
* Updates standalone-pi-mssql.xml and config.txt with node specific data.

### Setup Requirements

You'll need puppetlabs/stdlib, puppetlabs/dsc, and the xml_fragment modules.

### Beginning with centric8

The following yaml config prepares the environment for use.

```yaml
centric8::service_user:
  user: 'c8-service'
  domain: 'example'
  sensitive_password: 'secret-plaintext-passsword'
centric8::application_admin:
  user: 'Administrator
  sensitive_password: 'secret-plaintext-passsword1'
centric8::db_config:
  name: 'CSIC8'
  user: 'csidba'
  sesitive_password: 'secret-db-password'
  port: 1433
  server: 'C8DB01'
centric8::filevault_path: 'F:/filevault'
centric8::app_dns_name: 'centric8.example.com'
centric8::app_email: ''
centric8::app_server: 'C8APP01'
```

## Limitations

Currently only supports single application topologies which utilize MS SQL as
the backing database. Clustered database configs (or other platforms) are not
yet supported.

## Development

Make a pull request and we'll figure it out!
