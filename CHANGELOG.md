## 0.1.0 (2021-04-27)

### Feature (9 changes)

- [feat: convert to file_line for xml file](arden-puppet/arden-centric8@9c2817abc574b56d44fb6c937cbe52fa57369271)
- [feat: configure service account permissions](arden-puppet/arden-centric8@7d23d4e1a017e1584bb2aa34624ce18e1d800063)
- [feat: different regex for file_line](arden-puppet/arden-centric8@ca5b85af086edece498701fdc6a41977170cfc20)
- [feat: ensure appserver name is uppercase](arden-puppet/arden-centric8@672d65b2bf785207424b47176acdda662f7ab6e0)
- [feat: manage pi-configuration.properties](arden-puppet/arden-centric8@aae60ef2a5ca93d180a9edb402507de3ec5cb813)
- [feat: configure service state](arden-puppet/arden-centric8@d412ecc0625fb28e44a613fc0323f181bc7f9db5)
- [feat: minor renames of variables & hiera](arden-puppet/arden-centric8@02a35a0492aafd4704ec2e2fe682f3dafc2b65a5)
- [feat: ensure rexml is present](arden-puppet/arden-centric8@29207edb8a8bd70aa4807ceb324a5c6dfd6bc14d)
- [feat: initial implementation with tests](arden-puppet/arden-centric8@fca658ecaa5ef5358707fa4bab98f5775c7747c9)

### Bugfix (3 changes)

- [fix: xml_fragment attributes configuration](arden-puppet/arden-centric8@6d08d88ff8ec8b59862f42ad7d0677c805f05c0b)
- [fix: xml path for standalone-pi-mssql](arden-puppet/arden-centric8@c2d5329698d08f428a04493aebb80a51188453ff)
- [fix: lookup_options declaration](arden-puppet/arden-centric8@50de26e881a4849183cd54919b5a5337a2b9a61c)
